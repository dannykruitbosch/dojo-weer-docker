# Docker setup voor BiebCoders Weer Ballon

Deze repo bevat alle docker componenten voor het BiebCoders Weer Ballon project.
Met deze docker compositie is het mogelijk om alle componenten op jouw laptop of eigen server te draaien.

## Wat heb je nodig?

* Docker for Mac/Windows of Docker op een Linux machine [Get Docker!](https://www.docker.com/community-edition#/download)

## Wat zit hier allemaal in?

Er worden een aantal images gedownload en opgestart als je deze docker-compose file start.

- De Mosquitto MQTT server die de sensor data ontvangt vanuit het LoRa netwerk
- De Node-Red server die alle MQTT data verwerkt en op slaat in de database
- De MongoDB server waarin alle data wordt opgeslagen
- De NodeJS web applicatie die ons alle data laat zien zodat we de weer ballon kunnen volgen.

